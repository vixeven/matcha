import io from "socket.io";
import { User } from "../models";

io.on("connection", socket => {
  console.log("Socket ON");

  socket.on("disconnect", () => {
    console.log("Socket OFF");
  });

  socket.on("add-message", message => {
    io.emit("message", {
      text: message.text,
      id: message.id
    });
  });

  socket.on("log-in", id => {
    User.getUserById(id, (err, user) => {
      if (user)
        User.setOnline(user, error => {
          if (error) console.log(error);
          io.emit("log-in", {
            uid: user._id
          });
        });
    });
  });

  socket.on("log-out", id => {
    console.log(`das ${id}`);
    User.getUserById(id, (err, user) => {
      if (user)
        User.setOffline(user, error => {
          if (error) console.log(error);
          io.emit("log-out", {
            uid: user._id,
            time: Date.now()
          });
        });
    });
  });

  socket.on("isActive", id => {
    User.getUserById(id, (err, user) => {
      if (user) {
        socket.emit("onIsActive", user.online);
      }
    });
  });
});

export default io;
