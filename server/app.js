import cors from "cors";
import path from "path";
import http from "http";
import express from "express";
import passport from "passport";
import mongoose from "mongoose";
import bodyParser from "body-parser";
import fs from "fs";
import ioserv from "socket.io";
import auth from "./config/passport";
import config from "./config/database";
import controllers from "./controllers";
import { User, Chat } from "./models";

let users = {};

const app = express();
const port = 8085;
const server = http.createServer(app);
const io = ioserv(server);

io.on("connection", socket => {

    socket.on("add-message", data => {
      const chat = new Chat({
        to: data.to,
        author: data.from,
        message: data.message
      })
      Chat.addMessages(chat, (err, data) => {
        if (err) console.log(err);
          socket.broadcast.to(users[data.to]).emit('message', data);
      })
    });

    socket.on("seen", id => {
      Chat.seen(id);
    })

    socket.on("log-in", id => {
      User.getUserById(id, (err, user) => {
        if (err) console.log(err);
        if (user)
          User.setOnline(user, error => {
            if (error) console.log(error);
            socket.username = user.username;
            users[user._id] = socket.id;
            io.emit("log-in", {
              uid: user._id
            });
          });
      });
    });

    socket.on("log-out", id => {
      User.getUserById(id, (err, user) => {
        if (err) console.log(err);
        if (user)
          User.setOffline(user, error => {
            if (error) console.log(error);
            delete users[user._id];
            io.emit("log-out", {
              uid: user._id,
              time: Date.now()
            });
          });
      });
    });

    socket.on("disconnect", () => {
    });
});

app.set('io', io)
app.set('users', users)

if (!fs.existsSync(path.join(__dirname, "public"))) {
  fs.mkdirSync(path.join(__dirname, "public"));
}
if (!fs.existsSync(path.join(__dirname, "public", "photos"))) {
  fs.mkdirSync(path.join(__dirname, "public", "photos"));
}

auth(passport);

mongoose.connect(config.database, { useMongoClient: true });

mongoose.connection.on("connected", () => {
  console.log(`Connected to database ${config.database}`);
});

mongoose.connection.on("error", err => {
  console.log(`Connected to database ${err}`);
});

mongoose.Promise = global.Promise;

const corsOptions = {
  origin: ["http://localhost:4200"],
  methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
  optionsSuccessStatus: 204,
  preflightContinue: true
};
app.use(cors(corsOptions));

app.use(express.static(path.join(__dirname, "public")));
app.use(bodyParser.json());
app.use(controllers);


app.get("/", (req, res) => {
  res.send(`Conected users: ${Object.keys(users).length}`);
});

app.use((err, req, res, next) => {
  if (err) {
    if (err.name === "MongoError") {
      if (err.code === 11000) res.status(406).send("Duplicate key error");
    } else res.status(err.status || 400).send(err.message);
  } else {
    req.io = io;
    next();
  }
});

server.listen(port, () => console.log(`API running on localhost: ${port}`));
