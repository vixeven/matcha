import express from "express";
import passport from "passport";
import CreateError from "http-errors";
import { Alert, User, Chat } from "../models";

const router = express.Router();

router
  .post(
    "/report",
    passport.authenticate("jwt", { session: false }),
    (req, res, next) => {
      Alert.findReport(req.user._id, req.body.id, (err, report) => {
        if (err) throw err;
        if (report)
          return next(CreateError(403, "Ați raportat deja acest utilizator."));
        else if (!report) {
          const newReport = new Alert({
            from: req.user._id,
            to: req.body.id,
            scope: "report"
          });
          Alert.addAlert(newReport, error => {
            if (error) return next(CreateError.ServerError());
            return res.status(200).send("Utilizatorul a fost raporat.");
          });
        }
      });
    }
  )
  .post(
    "/block",
    passport.authenticate("jwt", { session: false }),
    (req, res, next) => {
      Alert.findBlock(req.user._id, req.body.id, (err, block) => {
        if (err) throw err;
        if (block) next(CreateError(403, "Ați blocat deja acest utilizator."));
        else {
          const newBlock = new Alert({
            from: req.user._id,
            to: req.body.id,
            scope: "block"
          });
          Alert.addAlert(newBlock, error => {
            if (error) return next(CreateError.ServerError());
            return res.status(201).send("Utilizatorul a fost blocat.");
          });
        }
      });
    }
  )
  .delete(
    "/block",
    passport.authenticate("jwt", { session: false }),
    (req, res, next) => {
      Alert.findBlock(req.user._id, req.headers.id, (err, block) => {
        if (err) throw err;
        if (!block)
          return next(CreateError(404, "Nu ați blocat acest utilizator."));
        // verifica dacă a trecut o zi: block.when și Date.now() --> 403 error
        return block.remove(error => {
          if (error) return next(CreateError(500, err));
          return res.status(200).send("Utilizatorul a fost deblocat.");
        });
      });
    }
  )
  .delete(
    "/like",
    passport.authenticate("jwt", { session: false }),
    (req, res, next) => {
      Alert.findLike(req.user._id, req.headers.url, (err, like) => {
        if (err) throw err;
        if (!like) return next(CreateError(404, "Like-ul nu a fost găsit."));
        return like.remove(error => {
          if (error) return next(CreateError(500, err));
          Alert.checkForLike(like.from, like.to, (errr, like1) => {
            if (errr) throw errr;
            Alert.checkForLike(like.to, like.from, (errr, like2) => {
                if (errr) throw errr;
                const users = req.app.get('users');
                const io = req.app.get('io');
                if (!like1 && like2) {
                  Alert.disconnect(like.to, like.from);
                    if (error) return next(CreateError(500, err));
                    if (like.to in users) {
                      User.getUserById(like.from, (err, user) => {
                        if (err) throw err;
                        io.to(users[like.to]).emit('notification', `Te-ai deonectat cu ${user.username}!`);
                      })
                    }
                    if (like.from) {
                      User.getUserById(like.to, (err, user) => {
                        if (err) throw err;
                        io.to(users[like.from]).emit('notification', `Te-ai deconectat cu ${user.username}!`);
                      })
                    }
                  }
              })
            })
          return res.status(200).send("Like-ul a fost eliminat.");
        });
      });
    }
  )
  .post(
    "/like",
    passport.authenticate("jwt", { session: false }),
    (req, res, next) => {
      Alert.findLike(req.user._id, req.body.url, (err, like) => {
        if (err) throw err;
        if (like) res.status(200).send("Like-ul există deja.");
        else {
          const newLike = new Alert({
            from: req.user._id,
            to: req.body.to,
            photo: req.body.url,
            scope: "like",
            message: `Aveți un like nou de la ${req.user.firstname} ${req.user.lastname}.`
          });

            const users = req.app.get('users');
            const io = req.app.get('io');

            Alert.checkForLike(req.body.to, req.user._id, (errr, like1) => {
              if (errr) throw errr;

              Alert.checkForLike( req.user._id, req.body.to, (error, like2) => {
                if (error) throw error;
                if (like1 && !like2) {
                  Alert.connect(req.body.to, req.user._id);
                    if (error) return next(CreateError(500, err));
                    if (req.body.to in users)
                      io.to(users[req.body.to]).emit('notification', `Te-ai conectat cu ${req.user.username}!`);
                    if (req.user._id in users) {
                      User.getUserById(req.body.to, (err, user) => {
                        if (err) throw err;
                        io.to(users[req.user._id]).emit('notification', `Te-ai conectat cu ${user.username}!`);
                      })
                    }
                }
                Alert.addAlert(newLike, e => {
                  if (e) return next(CreateError.ServerError());
                  io.to(users[req.body.to]).emit('notification', `Ai primit un like nou de la ${req.user.username}`);
                  return res.status(201).send("Like-ul a fost adăugat.");
                });

              })
            })

        }
      });
    }
  )
  .get(
    "/like",
    passport.authenticate("jwt", { session: false }),
    (req, res) => {
      const query = Alert.find({
        from: req.user._id,
        to: req.headers.to,
        scope: "like"
      }).select("photo -_id");

      query.exec((err, photos) => {
        res.status(200).json(photos);
      });
    }
  )
  .get(
    "/messages", passport.authenticate("jwt", { session: false }),
    (req, res, next) => {
      const to = req.user._id;
      const from = req.headers.from;
      User.getUsersById(from, (err, user) => {
        if (err) next(CreateError.ServerError())
        if(!user) {
          next(CreateError.ServerError())
        } else {
          Chat.getMessages(from, to, 30, (errr, messages) => {
            if (errr) next(CreateError.ServerError())
            res.status(200).send(messages);
          })
        }
    })
  })
  .get(
    "/chaters", passport.authenticate("jwt", { session: false }),
    (req, res) => {
      Alert.chaters(req.user._id, (err, users) => {
        if (err) return next(CreateError(500, err));
        if (!users)
          res.status(404);
        else {
          let to = [];
          to = users.map(item => item.to);
          User.getUsersById(to, (err, data) => {
            if (err) return next(CreateError(500, err));
            res.status(200).json(data);
          })
        }
    })
  })
  .get("/chater/:id", passport.authenticate("jwt", { session: false }),
  (req, res, next) => {
    Alert.findOne({from: req.user._id, to: req.params.id, scope: "connect" }, (err, data) => {
      if (err) return next(CreateError(500, err));
      if (!data) {
        return next(CreateError(404, "Hacker confirmed. xD"));
      }
      else {
        User.getUserById(data.to, (err, user) => {
          if (!user) {
            return next(CreateError(404, "FUCK THAT>>"));
          }
          else 
            res.status(200).json({
              firstname: user.firstname,
              lastname: user.lastname,
              username: user.username
            });
        })
      }
    })
  })
  .get(
    "/notifications", passport.authenticate("jwt", { session: false }),
    (req, res, next) => {
      Alert.notifications(req.user._id, (err, data) => {
        if (err) next(CreateError.ServerError())
        if (!data) {
          next(CreateError(404, "Nu aveti notificari."));
        } else {
          res.status(200).json(data);
        }
      })
  })
  .get(
    "/activites", passport.authenticate("jwt", { session: false }),
    (req, res, next) => {
      Alert.activites(req.user._id, (err, data) => {
        if (err) next(CreateError.ServerError())
        if (!data) {
          next(CreateError(404, "Nu aveti activitati."));
        } else {
          res.status(200).json(data);
        }
      })
  })
  .get(
    "/faim/:id",
    passport.authenticate("jwt", { session: false }),
    (req, res, next) => {
      const data = {};
      const to = req.params.id || req.user._id;

      Alert.find({ to, scope: "like"  }).count((err, count) => {
        data["likes"] = count;
        Alert.find({ to, scope: "guest"  }).count((err, count) => {
          data["guests"] = count;
          Alert.find({ to, scope: "connect"  }).count((err, count) => {
            data["connections"] = count;
            Chat.find({ to }).count((err, count) => {
              data["messages"] = count;
              User.getUserById(to, (err, user) => {
                user.fr = ((data["likes"] + data["guests"] + data["connections"] + data["messages"]) / 30).toPrecision(3);
                data["fr"] = user.fr;
                user.save(err => {
                  if (err)
                    throw err;
                  return res.status(200).send(data);
                })
              })
            })
          })
        })
      })




    }
  )
  .post(
    "/guest",
    passport.authenticate("jwt", { session: false }),
    (req, res, next) => {
      Alert.findGuest(req.user._id, req.body.to, (err, guest) => {
        if (err) throw err;
        if (guest) {
          guest.when = Date.now();
          guest.seen = false;
          guest.save(error => { 
            if (error) throw error;
            
            res.status(200).send("Vizită actualizată.");
          });
        } else {
          const newGuest = new Alert({
            from: req.user._id,
            to: req.body.to,
            scope: "guest",
            message: `${req.user.firstname} ${req.user.lastname} ti-a vizitat profilul.`
          });
          Alert.addAlert(newGuest, error => {
            if (error) return next(CreateError.ServerError());
            return res.status(201).send("Vizită adăugată.");
          });
        }
        const users = req.app.get('users');
        if (req.body.to in users) {
          const io = req.app.get('io');
          io.to(users[req.body.to]).emit('notification', `${req.user.firstname} ${req.user.lastname} te-a vizitat.`);
        }
      });
    }
  );

export default router;
