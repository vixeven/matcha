import express from "express";
import users from "./users";
import alerts from "./alerts";
import photos from "./photos";

const router = express.Router();

router.use("/alerts", alerts);
router.use("/photos", photos);
router.use("/users", users);

export default router;
