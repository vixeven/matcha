import mongoose from "mongoose";
import User from "./user";

const AlertSchema = mongoose.Schema({
  from: { type: String, required: true },
  to: { type: String, required: true },
  when: { type: Date, required: true, default: Date.now },
  photo: { type: String },
  scope: { type: String, required: true },
  seen: { type: Boolean, required: true, default: false },
  message: { type: String, required: true }
});

const Alert = mongoose.model("Alert", AlertSchema);

Alert.findGuest = (from, to, callback) => {
  Alert.findOne({ from, to, scope: "guest" }, callback);
};

Alert.findLike = (from, url, callback) => {
  Alert.findOne({ from, photo: url }, callback);
};

Alert.checkForLike = (from, to, callback) => {
  Alert.findOne({ from, to, scope: "like" }, callback);
}

Alert.chater = (from, to, callback) => {
  Alert.findOne({ from, to, scope: "like" }, callback);
};

Alert.findBlock = (from, to, callback) => {
  Alert.findOne({ from, to, scope: "block" }, callback);
};

Alert.findReport = (from, to, callback) => {
  Alert.findOne({ from, to, scope: "report" }, callback);
};

Alert.addAlert = (newNotification, callback) => {
  newNotification.save(callback);
};

Alert.connect = (user1, user2) => {

  User.findById(user1, (err, user) => {
    const not1 = new Alert({
      from: user1,
      to: user2,
      scope: 'connect',
      message: `Tu și ${user.firstname} ${user.lastname} sunteți conectați acum.`
    })
    not1.save(err => { if (err) { console.log(err); } })
  })

  User.findById(user2, (err, user) => {
    const not2 = new Alert({
      from: user2,
      to: user1,
      scope: 'connect',
      message: `Tu și ${user.firstname} ${user.lastname} sunteți conectați acum.`
    })
    not2.save(err => { if (err) { console.log(err); } })
  })
  
}

Alert.disconnect = (user1, user2) => {

  Alert.findOne({from: user1, to: user2, scope: "connect"}, (err, connection) => {
    connection.remove(err => {
      if (err) console.log(err);
      User.findById(user1, (err, user) => {
        const not1 = new Alert({
          from: user1,
          to: user2,
          scope: 'disconnect',
          message: `Tu și ${user.firstname} ${user.lastname} vați deconectat.`
        })
        not1.save(err => { if (err) { console.log(err); } })
      })
    })
  })

  Alert.findOne({from: user2, to: user1, scope: "connect"}, (err, connection) => {
    connection.remove(err => {
      if (err) console.log(err);
      User.findById(user2, (err, user) => {
        const not2 = new Alert({
          from: user2,
          to: user1,
          scope: 'disconnect',
          message: `Tu și ${user.firstname} ${user.lastname} vați deconectat.`
        })
        not2.save(err => { if (err) { console.log(err); } })
      })
    })
  })

}

Alert.seen = to => {
  Alert.update({to, seen: false}, {seen: true}, {multi: true}, (err) => {
    if (err) console.log(err)
  })
}

Alert.chaters = (from, callback) => {
  Alert.find({
        from,
        scope: "connect"
      })
      .select("to -_id")
      .exec(callback)
}

Alert.notifications = (to, callback) => {
  Alert.find({
    to
  })
  .sort({ when: -1 })
  .select("when message scope -_id")
  .exec(callback)
}

Alert.activites = (user, callback) => {
  Alert.find({
    from: user
  })
  .sort({ when: -1 })
  .select("when message scope -_id")
  .exec(callback)
}

export default Alert;
