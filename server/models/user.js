import mongoose from "mongoose";
import bcrypt from "bcryptjs";

const Schema = mongoose.Schema;
const tmp = new Date(Date.now() - 567648000000);
const maxdate = `${tmp.getFullYear()}-${tmp.getMonth()}-${tmp.getDay()}`;
const UserSchema = new Schema({
  lastname: { type: String, required: true },
  firstname: { type: String, required: true },
  email: {
    type: String,
    required: true,
    unique: [true, "Email must be unique, this exists."],
    dropDups: true
  },
  username: {
    type: String,
    required: true,
    unique: [true, "Username must be unique, this exists."],
    dropDups: true
  },
  password: { type: String, required: true },
  gender: { type: String, default: "-" },
  preferences: { type: String, default: "-" },
  birthdate: { type: Date, max: Date(maxdate) },
  photo: { type: String },
  biography: { type: String },
  tags: { type: String },
  location: { type: String },
  lat: { type: String },
  lng: { type: String },
  filled: { type: Boolean, required: true, default: false },
  fr: { type: Number, required: true, default: 0},
  online: { type: String, required: true, default: "-" }
});

UserSchema.pre("save", function check(next) {
  const user = this;
  if (!user.isModified("password")) return next();
  return bcrypt.genSalt(10, (err, salt) => {
    bcrypt.hash(user.password, salt, (error, hash) => {
      if (error) return next(err);
      user.password = hash;
      return next();
    });
  });
});

const User = mongoose.model("User", UserSchema);

User.getUserById = (id, callback) => {
  User.findById(id, callback);
};

User.getUsersById = (array, callback) => {
  User
    .find( { "_id" : { $in: array } },
    "firstname lastname username birthdate photo",
    callback );
}

User.getUserByUsername = (username, callback) => {
  const query = { username };

  User.findOne(query, callback);
};

User.addUser = newUser => newUser.save();

User.setOffline = (user, callback) => {
  user.online = Date.now();
  user.save(callback);
};

User.setOnline = (user, callback) => {
  user.online = "Y";
  user.save(callback);
};

User.comparePassword = (candidatePassword, hash, callback) => {
  bcrypt.compare(candidatePassword, hash, (err, isMatch) => {
    if (err) console.log(err);
    callback(null, isMatch);
  });
};

export default User;
