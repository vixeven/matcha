import mongoose from "mongoose";
import { Alert } from ".";

const ChatSchema = mongoose.Schema({
  author: { type: String, required: true },
  to: { type: String, required: true },
  message: { type: String, required: true },
  seen: { type: Boolean, default: false },
  date: { type: Date, required: true, default: Date.now }
});

const Chat = mongoose.model("Chat", ChatSchema);

Chat.getMessages = (author, to, limit, callback) => {
  Chat.find({})
    .or([
      { $and: [{ author, to }] },
      { $and: [{ author: to, to: author }] }
    ])
    .sort({ date: -1 })
    .limit(limit)
    .exec(callback);
  Chat.update({author, to, seen: false}, {seen: true}, {multi: true}, (err) => {
    if (err) console.log(err)
  })
};

Chat.addMessages = (newMessage, callback) => {
  newMessage.save(callback);
};

Chat.seen = _id => {
  Chat.update({_id}, { seen:true }, (err) => {
    if (err) console.log(err);
  })
}

export default Chat;
