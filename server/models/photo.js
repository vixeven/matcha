import mongoose from "mongoose";

const PhotoSchema = mongoose.Schema({
  address: { type: String, required: true },
  authorId: { type: String, required: true },
  update: { type: Date, required: true, default: Date.now }
});

const Photo = mongoose.model("Photo", PhotoSchema);

Photo.getPhotosByAuthorId = (authorId, callback) => {
  Photo.find({ authorId }, callback);
};

Photo.addPhoto = (newPhoto, callback) => {
  newPhoto.save(callback);
};

export default Photo;
