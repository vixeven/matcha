import { NgMatchaPage } from './app.po';

describe('ng-matcha App', () => {
  let page: NgMatchaPage;

  beforeEach(() => {
    page = new NgMatchaPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
