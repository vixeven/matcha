import { Injectable, Inject } from '@angular/core';
import { Subject }    from 'rxjs/Subject';

@Injectable()
export class MessageService {

  private newMessage = new Subject<any>();
  messageObservable$ = this.newMessage.asObservable();
  public message(data: any) {
    if (data) {
      this.newMessage.next(data);
    }
  }

  private newNotification = new Subject<any>();
  notificationObservable$ = this.newNotification.asObservable();
  public notification(data: any) {
    if (data) {
      this.newNotification.next(data);
    }
  }

}