import { Injectable } from "@angular/core";

@Injectable()
export class ValidateService {
  constructor() {}

  validateRegister(user) {
    if (
      user.lastname === undefined ||
      user.firstname === undefined ||
      user.username === undefined ||
      user.gender === undefined
    ) {
      return false;
    } else {
      return true;
    }
  }

  validateUpdate(user) {
    if (!this.validateRegister(user)) {
      return false;
    }
    if (
      user.lastname === "" ||
      user.firstname === "" ||
      user.username === "" ||
      user.gender === "" ||
      user.biography === "" ||
      user.lat === "" ||
      user.lat === undefined
    ) {
      return false;
    }
    return true;
  }

  validateLogin(user) {
    if (user.username === undefined || user.password === undefined) {
      return false;
    } else {
      return true;
    }
  }

  validateEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  validatePassword(password) {
    const re = /^(?=.*?[A-Za-z])(?=.*?[0-9])(?=.*?[#?!@.$%^&*-]).{8,}$/;
    return re.test(password);
  }

  validatePasswords(first, second) {
    return first === second;
  }
}
