import "rxjs/add/operator/map";
import { Injectable } from "@angular/core";
import { Http, Headers } from "@angular/http";
import { tokenNotExpired } from "angular2-jwt";
import { environment } from "../../environments/environment";

@Injectable()
export class AuthService {
  authToken: any;
  user: any;
  isDev: boolean;

  constructor(private http: Http) {
    this.isDev = false;
  }

  registerUser(user) {
    const headers = new Headers();
    headers.append("Content-Type", "application/json");
    const ep = this.prepEndpoint("users/register");
    return this.http.post(ep, user, { headers: headers });
  }

  authenticateUser(user) {
    const headers = new Headers();
    headers.append("Content-Type", "application/json");
    const ep = this.prepEndpoint("users/authenticate");
    return this.http.post(ep, user, { headers: headers });
  }

  getProfile() {
    const headers = new Headers();
    this.loadToken();
    headers.append("Authorization", this.authToken);
    headers.append("Content-Type", "application/json");
    const ep = this.prepEndpoint("users/profile");
    return this.http.get(ep, { headers: headers }).map(res => res.json());
  }

  updateProfile(user) {
    const headers = new Headers();
    this.loadToken();
    headers.append("Authorization", this.authToken);
    headers.append("Content-Type", "application/json");
    const ep = this.prepEndpoint("users/updateprofile");
    return this.http.put(ep, user, { headers: headers });
  }

  storeUserData(token, user) {
    localStorage.setItem("id_token", token);
    localStorage.setItem("user", JSON.stringify(user));
    this.authToken = token;
    this.user = user;
  }

  isFilled() {
    const headers = new Headers();
    this.loadToken();
    headers.append("Authorization", this.authToken);
    headers.append("Content-Type", "application/json");
    const ep = this.prepEndpoint("users/filled");
    return this.http.get(ep, { headers: headers });
  }

  loadToken() {
    const token = localStorage.getItem("id_token");
    this.authToken = token;
  }

  loggedIn() {
    return tokenNotExpired("id_token");
  }

  logout() {
    this.authToken = null;
    this.user = null;
    localStorage.clear();
  }

  prepEndpoint(ep) {
    if (this.isDev) {
      return ep;
    } else {
      return environment.API_URL + ep;
    }
  }
}
