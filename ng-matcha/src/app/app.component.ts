import { Component, Injectable, OnInit, HostListener } from "@angular/core";
import { SocketService } from "./services/socket.service";
import { Http, Headers } from "@angular/http";

import * as Materialize from "angular2-materialize";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
  providers: [SocketService]
})
@Injectable()
export class AppComponent implements OnInit {
  connection;

  constructor(private socket: SocketService, private http: Http) {}

  @HostListener("window:unload", ["$event"])
  unloadHandler(event) {
    if (localStorage.user) {
      this.socket.setOffline();
    }
  }

  ngOnInit() {
    if (localStorage.user) {
      this.socket.setOnline();
    }
  }
}
