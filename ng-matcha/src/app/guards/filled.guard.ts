import { Injectable } from "@angular/core";
import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from "@angular/router";
import { Observable } from "rxjs/Rx";
import { AuthService } from "../services/auth.service";
import "rxjs/add/operator/catch";

import * as Materialize from "angular2-materialize";

@Injectable()
export class FilledGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}

  canActivate() {
    return this.authService
      .isFilled()
      .map(res => {
        if (res.ok) {
          return true;
        }
      })
      .catch((err: Response, caught: Observable<boolean>) => {
        if (err.status === 404) {
          Materialize.toast("Actualizați profilul pentru a continua.", 3000);
          this.router.navigate(["/edit"]);
          return Observable.throw("Complete your profile");
        }
        return Observable.throw(caught);
      });
  }
}
