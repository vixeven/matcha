import { Injectable } from "@angular/core";
import { Router, CanActivate } from "@angular/router";
import { AuthService } from "../services/auth.service";

import * as Materialize from "angular2-materialize";

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}

  canActivate() {
    if (this.authService.loggedIn()) {
      return true;
    } else {
      Materialize.toast("Nu sunteți autentificat.", 3000);
      this.router.navigate(["/login"]);
      return false;
    }
  }
}
