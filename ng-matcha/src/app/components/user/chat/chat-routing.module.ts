import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { ChatComponent } from "./chat.component";
import { MessagesComponent } from "./messages.component";

import { AuthGuard } from "../../../guards/auth.guard";
import { FilledGuard } from "../../../guards/filled.guard";

const profilesRoutes: Routes = [
  {
    path: "chat",
    component: ChatComponent,
    canActivate: [AuthGuard, FilledGuard]
  },
  {
    path: "chat/t/:id",
    component: MessagesComponent,
    canActivate: [AuthGuard, FilledGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(profilesRoutes)],
  exports: [RouterModule]
})
export class ChatRoutingModule {}
