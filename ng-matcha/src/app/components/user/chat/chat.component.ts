import { Component, OnInit } from "@angular/core";
import { ChatService } from "./chat.service";


@Component({
  selector: "app-chat",
  templateUrl: "./chat.component.html",
  styleUrls: ["./chat.component.css"]
})
export class ChatComponent implements OnInit {
  chaters;
  loading = true;
  constructor(private chatService: ChatService) {}
  ngOnInit() {
    this.chatService.getChaters().subscribe(chaters => {
      this.chaters = chaters;
      this.loading = false;
    });
  }
}
