import { Http, Headers } from "@angular/http";
import { Injectable } from "@angular/core";
import { AuthService } from "../../../services/auth.service";
import { Observable } from "rxjs/Observable";
import { Router } from "@angular/router";

@Injectable()
export class ChatService {

  constructor(
    private authService: AuthService,
    private http: Http,
    private router: Router
  ) {}

  getChaters() {
    const headers = new Headers();
    this.authService.loadToken();
    headers.append("Authorization", this.authService.authToken);
    headers.append("Content-Type", "application/json");
    const ep = this.authService.prepEndpoint("alerts/chaters");
    return this.http.get(ep, { headers: headers }).map(res => res.json());
  }

  getMessages(chater) {
    const headers = new Headers();
    this.authService.loadToken();
    headers.append("Authorization", this.authService.authToken);
    headers.append("from", chater)
    headers.append("Content-Type", "application/json");
    const ep = this.authService.prepEndpoint("alerts/messages");
    return this.http.get(ep, { headers: headers }).map(res => res.json());
  }

  getCheater(id) {
    const headers = new Headers();
    this.authService.loadToken();
    headers.append("Authorization", this.authService.authToken);
    headers.append("Content-Type", "application/json");
    const ep = this.authService.prepEndpoint("alerts/chater");
    return this.http.get(`${ep}/${id}`, { headers: headers }).map(res => res.json());
  }

  isCheater(id) {
    this.getChaters().subscribe(chaters => {
      if (chaters.includes(id) === false) {
        this.router.navigate(["/chat"]);
      }
    });
  }
}
