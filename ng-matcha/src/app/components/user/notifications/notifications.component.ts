import { Component, OnInit, OnDestroy } from '@angular/core';
import { MessageService } from "../../../services/message.service";
import { NotificationsService } from "../../../services/notifications.service";

import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit, OnDestroy {

  private subscription: Subscription;

  notifications = [];
  activites = [];

  constructor(
    private message: MessageService,
    private notification: NotificationsService
  ) { }

  add_notification(title, date, type) {
    this.notifications.unshift({title, date, type})
  }

  load_activites() {
    this.activites = [];
    this.notification.getActivity().subscribe(activites => {
      for(let activite of activites) {
        this.activites.push({
          title: activite.message,
          date: activite.when,
          type: activite.scope
        })
      }
    })
  }

  ngOnInit() {
    this.notification.getNotifications().subscribe(data => {
      console.log(data);
      for(let item of data) {
        this.notifications.push({
          title: item.message,
          date: item.when,
          type: item.scope
        })
      }
    })
    this.subscription = this.message.notificationObservable$.subscribe(data => {
      this.add_notification(data, Date.now(), "guest")
    })
  }
  
  ngOnDestroy () {
    this.subscription.unsubscribe();
  }

}
