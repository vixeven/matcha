import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { MaterializeModule } from "angular2-materialize";

import { ProfileComponent } from "./profile.component";
import { ProfileListComponent } from "./profile-list.component";
import { FilterComponent } from "./filter.component";

import { ProfileService } from "./profile.service";

import { ProfileRoutingModule } from "./profiles-routing.module";

@NgModule({
  imports: [MaterializeModule, CommonModule, FormsModule, ProfileRoutingModule],
  declarations: [ProfileListComponent, ProfileComponent, FilterComponent],
  providers: [ProfileService]
})

export class ProfilesModule {}
