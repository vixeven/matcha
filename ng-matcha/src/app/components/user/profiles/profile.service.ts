import { Injectable } from "@angular/core";
import { AuthService } from "../../../services/auth.service";
import * as $ from "jquery";

export class Profile {
  constructor(
    public id: string,
    public username: string,
    public lastname: string,
    public firstname: string,
    public biography: string,
    public gender: string,
    public photo: string,
    public birthdate: string,
    public location: string,
    public preferences: string
  ) {}
}

// let src = AuthService.prepEndpoint('users/tags/' + this.username);

const PROFILES = [];

const profilesPromise = Promise.resolve(PROFILES);

const src = "http://localhost:8085/users/all";
$.getJSON(src, data => {
  data.forEach(function(item) {
    PROFILES.push(
      new Profile(
        item.id,
        item.username,
        item.firstname,
        item.lastname,
        item.biography,
        item.gender,
        item.photo,
        item.birthdate,
        item.location,
        item.preferences
      )
    );
  });
});

@Injectable()
export class ProfileService {
  getProfiles() {
    return profilesPromise;
  }
}
