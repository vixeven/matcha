import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { ProfileComponent } from "./profile.component";
import { ProfileListComponent } from "./profile-list.component";
import { HomeComponent } from "../../website/home/home.component";

import { AuthGuard } from "../../../guards/auth.guard";
import { FilledGuard } from "../../../guards/filled.guard";

const profilesRoutes: Routes = [
  {
    path: "browse",
    component: ProfileListComponent,
    canActivate: [AuthGuard, FilledGuard]
  },
  {
    path: "profile/:username",
    component: ProfileComponent,
    canActivate: [AuthGuard, FilledGuard]
  },
  {
    path: "profile",
    component: ProfileListComponent,
    canActivate: [AuthGuard, FilledGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(profilesRoutes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule {}
