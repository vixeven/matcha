import { Component, OnInit, HostBinding, OnDestroy } from "@angular/core";
import { Router, ActivatedRoute, Params } from "@angular/router";

import { Profile, ProfileService } from "./profile.service";
import { BrowseService } from "../../../services/browse.service";
import { SocketService } from "../../../services/socket.service";
import { AuthService } from "../../../services/auth.service";

import { Observable } from "rxjs/Rx";
import "rxjs/add/operator/switchMap";

import * as Materialize from "angular2-materialize";

@Component({
  templateUrl: "./profile.component.html",
  styleUrls: ["./profile.component.css", "../upload/upload.component.css"]
})
export class ProfileComponent implements OnInit, OnDestroy {
  profile: any;
  connection;
  countrent: any;
  imageURLs: any;
  liked: any = [];
  username: any;
  status: any;
  tags: any;
  myId = JSON.parse(localStorage.user)._id;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private socket: SocketService,
    private service: ProfileService,
    private browseService: BrowseService,
    private authService: AuthService
  ) {}

  userStatus() {
    this.browseService
      .checkUser(this.username)
      .map(res => {
        if (res.status !== 403 && res.status !== 404) return res.json();
      })
      .catch((err: Response, caught: Observable<boolean>) => {
        if (err.status === 404) {
          Materialize.toast("Utilizator inexistent.", 3000, "red");
          this.router.navigate(["/browse"]);
        } else if (err.status === 403) {
          Materialize.toast("Utilizatorul nu este disponibil.", 3000, "blue");
          this.router.navigate(["/browse"]);
        }
        return Observable.throw(caught);
      })
      .subscribe(profile => {
        if (profile) {
          this.profile = profile;
          this.tags = this.profile.tags.split(",");
          this.tags.slice(0, 1);
          this.loadPhotos(profile.id);
          if (this.profile.id !== this.myId) {
            this.browseService.getLikes(profile.id).subscribe(data => {
              this.liked = data.map(item => item["photo"]);
            });
            this.browseService.guest(this.profile.id).subscribe(data => {
              console.log(data["_body"])
            })
          }
          this.socket.getUserOnline(this.profile.id);
        }
      });
  }

  ngOnInit() {
    const self = this;
    this.route.params.subscribe(params => {
      this.username = params["username"];
      this.userStatus();
    });
    this.socket.socket.on("log-in", data => {
      if (data.uid === self.profile.id) {
        self.profile.online = "Y";
      }
    });
    this.socket.socket.on("log-out", data => {
      if (data.uid === self.profile.id) {
        self.profile.online = data.time;
      }
    });
  }

  ngOnDestroy() {
  }

  like(url) {
    if (this.liked.indexOf(url) !== -1) {
      this.browseService.unlike(url).subscribe(data => {
        Materialize.toast(data["_body"], 333);
      });
      this.liked.splice(this.liked.indexOf(url), 1);
    } else {
      this.browseService.like(this.profile.id, url).subscribe(data => {
        Materialize.toast(data["_body"], 333);
      });
      this.liked.push(url);
    }
  }

  block() {
    this.browseService.block(this.profile.id).subscribe(data => {
      Materialize.toast(data["_body"], 700);
    });
    this.profile.block = true;
  }

  report() {
    this.browseService.report(this.profile.id).subscribe(
      data => Materialize.toast(data["_body"], 700),
      error => {
        Materialize.toast(error["_body"], 700);
      }
    );
    this.profile.block = true;
  }

  unblock() {
    this.browseService.unblock(this.profile.id).subscribe(data => {
      Materialize.toast(data["_body"], 700);
    });
    this.profile.block = false;
  }

  isPhotoLiked(url) {
    return this.liked.indexOf(url) !== -1 ? "red" : "grey";
  }

  loadPhotos(id) {
    const self = this;

    this.browseService.getAllPhotos(id).subscribe(images => {
      self.imageURLs = images.map(image =>
        this.authService.prepEndpoint(
          image.address.slice(1, image.address.length)
        )
      );
      self.countrent = self.imageURLs.length;
    });
  }

  gotoProfiles() {
    this.router.navigate(["/browse"]);
  }
}
