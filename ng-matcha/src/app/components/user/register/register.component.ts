import { Router } from "@angular/router";
import { Component, OnInit } from "@angular/core";
import { AuthService } from "../../../services/auth.service";
import { ValidateService } from "../../../services/validate.service";

import * as Materialize from "angular2-materialize";

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.css"]
})
export class RegisterComponent implements OnInit {
  lastname: String;
  firstname: String;
  username: String;
  email: String;
  password: String;
  repeat: String;
  birthdate: String;
  gender: String;
  maxdate: Array<Number>;
  valid: boolean;

  constructor(
    private validateService: ValidateService,
    private authService: AuthService,
    private router: Router
  ) {}

  ngOnInit() {
    this.valid = true;
    const aux = new Date(Date.now() - 567611000000);
    this.maxdate = [aux.getFullYear(), aux.getMonth(), aux.getDay()];
  }

  onRegisterSubmit() {
    const user = {
      lastname: this.lastname,
      firstname: this.firstname,
      username: this.username,
      email: this.email,
      password: this.password,
      repeat: this.repeat,
      birthdate: this.birthdate,
      gender: this.gender
    };

    if (!this.validateService.validateRegister(user)) {
      this.valid = false;
      Materialize.toast(
        "Vă rugăm să completați toate câmpurile.",
        1300,
        "",
        () => {
          this.valid = true;
        }
      );
      return false;
    } else if (!this.validateService.validateEmail(user.email)) {
      this.valid = false;
      Materialize.toast("Adresa de e-mail este invalidă.", 1300, "", () => {
        this.valid = true;
      });
      return false;
    } else if (!this.validateService.validatePassword(user.password)) {
      this.valid = false;
      Materialize.toast(
        "Parola trebuie să conțină cel puțin o cifră și o literă.",
        1300,
        "",
        () => {
          this.valid = true;
        }
      );
      return false;
    } else if (
      !this.validateService.validatePasswords(user.password, user.repeat)
    ) {
      this.valid = false;
      Materialize.toast("Parolele nu coincid.", 1300, "", () => {
        this.valid = true;
      });
      return false;
    }

    this.authService.registerUser(user).subscribe(data => {
      if (data.ok) {
        Materialize.toast(
          "Contul a fost creat cu succes. Redirecționare...",
          1000,
          "",
          this.router.navigate(["/login"])
        );
      } else {
        Materialize.toast(
          "Ceva a mers rău. Încercați din nou.",
          1500,
          "",
          this.router.navigate(["/register"])
        );
      }
    });
  }
}
