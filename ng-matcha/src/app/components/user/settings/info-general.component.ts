import { Component, OnInit } from "@angular/core";
import { AuthService } from "../../../services/auth.service";
import { Router, ActivatedRoute, Params } from "@angular/router";
import { ValidateService } from "../../../services/validate.service";
import { SocketService } from "../../../services/socket.service";
import * as Materialize from "angular2-materialize";

@Component({
  selector: "app-info-general",
  templateUrl: "./info-general.component.html",
  styleUrls: ["./info-general.component.css"]
})
export class InfoGeneralComponent implements OnInit {
  user: any;

  public options = { types: ["address"] };
  getAddress(place: Object) {
    console.log("Address", place);
  }

  constructor(
    private authService: AuthService,
    private route: ActivatedRoute,
    private router: Router,
    private socket: SocketService,
    private validateService: ValidateService
  ) {}

  ngOnInit() {
    this.authService.getProfile().subscribe(
      profile => {
        this.user = profile;
      },
      err => {
        console.error(err);
        return false;
      }
    );
  }

  onUpdateSubmit() {
    // trebuie de updatead si in localStorage matinca :D
    if (!this.validateService.validateUpdate(this.user)) {
      Materialize.toast("Completați toate câmpurile.", 2000);
      return false;
    }
    console.log(this.user);
    this.authService.updateProfile(this.user).subscribe(
      data => {
        Materialize.toast(data.text(), 2000, "");
      },
      err => {
        console.error(err);
        Materialize.toast(err, 2000, "");
        return false;
      }
    );
  }
}
