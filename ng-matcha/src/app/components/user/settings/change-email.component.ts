import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { SettingsService } from '../../../services/settings.service';
import { ValidateService } from '../../../services/validate.service';

import * as Materialize from "angular2-materialize";

@Component({
  selector: 'app-change-email',
  templateUrl: './change-email.component.html',
  styleUrls: ['./change-email.component.css']
})
export class ChangeEmailComponent implements OnInit {

  currentpassword:String;
  newemail:       String;
  valid:          boolean = true;
  user:           any;

  constructor(private validateService: ValidateService,
              private settingsService: SettingsService,
              private router:          Router,
              private authService:     AuthService) { }

  ngOnInit() {
    this.authService.getProfile().subscribe(profile => {
      this.user = profile.user;
    }, err => {
      console.error(err);
      return false;
    })
  }

  onChangeEmail() {
    if(!this.currentpassword || !this.newemail){
      this.valid = false
      Materialize.toast("Completați toate câmpurile.", 1300, '',()=> {
        this.valid = true
      })
      return false
    }
    if(!this.validateService.validateEmail(this.newemail)){
      this.valid = false
      Materialize.toast("Adresa de e-mail este invalidă.", 1300, '',()=> {
        this.valid = true
      })
      return false
    }

    else if(!this.validateService.validatePassword(this.currentpassword)){
      this.valid = false
      Materialize.toast("Parolă incorectă.", 1300, '',()=> {
        this.valid = true
      })
      return false
    }

    this.settingsService.changeEmail(this.user,this.currentpassword,this.newemail).subscribe(data => {

      if(data.success) {
          this.currentpassword=this.newemail=undefined;
          Materialize.toast(data.msg, 1000,'')
      } else {
          Materialize.toast(data.msg, 1500,)
      }

    })
  }

}
