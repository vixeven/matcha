import { Router } from "@angular/router";
import { Component, OnInit } from "@angular/core";
import { AuthService } from "../../../services/auth.service";
import { SettingsService } from "../../../services/settings.service";
import { ValidateService } from "../../../services/validate.service";

import * as $ from "jquery";
import * as Materialize from "angular2-materialize";

@Component({
  selector: "app-tags",
  templateUrl: "./tags.component.html",
  styleUrls: ["./tags.component.css"]
})
export class TagsComponent implements OnInit {
  chipsInit: Object;
  username: String;
  password: String;
  repeat: String;
  old: String;
  success: any;
  user: any;

  valid = true;

  constructor(
    private validateService: ValidateService,
    private settingsService: SettingsService,
    private router: Router,
    private authService: AuthService
  ) {
    this.username = JSON.parse(localStorage.user).username;
    let src = this.authService.prepEndpoint("users/tags/" + this.username);
    $.getJSON(src).then(data => {
      var initTag = [];
      var tags = data.split(",");
      for (let i = 1; i < tags.length; i++) initTag.push({ tag: tags[i] });
      this.chipsInit = {
        data: initTag
      };
    });
  }

  ngOnInit() {
    this.authService.getProfile().subscribe(
      profile => {
        this.user = profile.user;
      },
      err => {
        return false;
      }
    );
  }

  add(chip) {
    this.settingsService.addTag(chip.tag, this.username).subscribe(
      res => {
        console.log(res.msg);
      },
      err => {
        return false;
      }
    );
  }

  delete(chip) {
    this.settingsService.removeTag(chip.tag, this.username).subscribe(
      res => {
        console.log(res.msg);
      },
      err => {
        return false;
      }
    );
  }
}
