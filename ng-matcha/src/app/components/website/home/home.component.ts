import { Component, OnInit } from '@angular/core';

import { BrowseService } from '../../../services/browse.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  user: any;
  constructor(
    private service: BrowseService
  ) { }

  ngOnInit() {
    this.service.getFameRating(JSON.parse(localStorage.getItem("user"))._id).subscribe(data => {
      this.user = data.json();
    })
  }

}
