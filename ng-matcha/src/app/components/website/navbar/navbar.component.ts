import { Component, OnInit } from "@angular/core";
import { AuthService } from "../../../services/auth.service";
import { SocketService } from "../../../services/socket.service";
import { Router } from "@angular/router";
import { MessageService } from '../../../services/message.service';
import * as Materialize from "angular2-materialize";

@Component({
  selector: "app-navbar",
  templateUrl: "./navbar.component.html",
  styleUrls: ["./navbar.component.css"]
})
export class NavbarComponent implements OnInit {
  constructor(
    public authService: AuthService,
    private socket: SocketService,
    private router: Router,
    private message: MessageService
  ) {}

  active = 0;
  notifications = 0;
  messages = 0;

  reset_notifications() {
    this.notifications = 0;
  }

  reset_messages() {
    this.messages = 0;
  }
  
  ngOnInit() {

    if(this.authService.loggedIn()) {
      this.socket.getNotification().subscribe(data => {
        let pathname = window.location.pathname;
        if (pathname.includes("/notifications")) {
          this.message.notification(data);
        } else {
          this.notifications++;
          if (this.active < 4) {
            this.active++;
            Materialize.toast(data, 666, '', () => {
              this.active--;
            });
          }
        }
      })
      this.socket.getMessages().subscribe(data => {
        let pathname = window.location.pathname;
        if (pathname.includes("/chat/t/") && pathname.slice(8) == data["author"]) {
            this.message.message(data);
          } else {
            this.messages++;
            if (this.active < 4) {
              this.active++;
              Materialize.toast("Aveti un mesaj nou.", 666, '', () => {
                this.active--;
              });
            }
          }
      })
    }
  }

  onLogoutClick() {
    this.socket.setOffline();
    this.authService.logout();
    Materialize.toast("Vați delogat cu succes.", 900, "", () => {
      this.router.navigate(["/"]);
    });
  }

  readNotifications() {
    this.notifications = 0;
  }
}
